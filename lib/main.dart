import 'package:flutter/material.dart';
import 'articles.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(NewsFeed());

class NewsFeed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'News Feed Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: MyHomePage(title: 'News Feed Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  List <Article> _articles = Article.generateArticles();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new RefreshIndicator(
        onRefresh: () async{
          await new Future.delayed(const Duration(seconds: 1));
          setState(() {
            _articles.removeAt(0);            
          });
          // Scaffold
          //   .of(context)
          //   .showSnackBar(new SnackBar(content: Text('Refreshed')));
          //   return;
        },
        child: _manufactureBody(),
      )
    );
  }


  Widget _manufactureBody(){
    return new ListView(
        children: _listTiles()
    );
  }

  List<Widget> _listTiles(){
    List <Widget> articleCollector = [];
    for (int i = 0; i < _articles.length; i++) {
      articleCollector.add(
            Padding(
              padding: EdgeInsets.all(16.0),
              child: ExpansionTile(
                title: Text(_articles[i].title, style: new TextStyle(fontSize: 24.0, color: Colors.orange)),
                children: <Widget>[
                  Text(_articles[i].commentCount),
                  IconButton(
                    icon: Icon(Icons.launch),
                    color: Colors.red,
                    onPressed: ()async{
                      const url = "https://www.google.com";
                      if (await canLaunch(url)){
                        launch(url);
                      }
                      else{
                        throw("Cannot launch $url");
                      }
                    },
                  )
                ]
              )
            )
        );
      } 
    return articleCollector;
  }
}
