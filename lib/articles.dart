class Article{
  final String title;
  final String link;
  final String commentCount;

  const Article({this.title, this.link, this.commentCount});

  static List<Article> generateArticles(){
    List<Article> articleCollector = [];
    for (var i = 0; i < 100; i++) {
      articleCollector.add(new Article(title: ('Article number ' + i.toString()),
                                        link: ('Link number ' + i.toString()),
                                        commentCount: ('Comment count ' + i.toString())
                                        ));
    }
    return articleCollector;
  }
}
